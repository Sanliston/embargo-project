package com.embargo.sanliston.embargoproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.embargo.sanliston.embargoproject.ListViewLogic.CustomListAdapter;
import com.embargo.sanliston.embargoproject.ListViewLogic.ElementHolder;
import com.embargo.sanliston.embargoproject.ListViewLogic.ListViewLogic;
import com.facebook.drawee.view.SimpleDraweeView;

public class BrowseFragment extends Fragment{

    //instance variables
    private String title;
    private int page;

    //InstantiateTheViews

    //constructor
    public static BrowseFragment newInstance(int page, String title){
        BrowseFragment browseFragment = new BrowseFragment();

        //storing values into bundle so they can be accessed later
        Bundle bundle = new Bundle();
        bundle.putInt("browseInt", page);
        bundle.putString("browseTitle", title);
        browseFragment.setArguments(bundle);

        return browseFragment;

    }//constructor

    //store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("browseInt", 0);
        title = getArguments().getString("browseTitle");
    }//onCreate

    //Inflate the view using the XML for the fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.browse_fragment, container, false); //come back to this part when you create the XML for the fragments

        ListView listView = (ListView)view.findViewById(R.id.browse_list_view); //This approach stops nullpointerexceptions
        CustomListAdapter customListAdapter = (CustomListAdapter) ListViewLogic.storage.get("customListAdapter");
        listView.setAdapter(customListAdapter);
        Log.i("BrowseFragment","Size of ArrayList after adapter is set: "+ListViewLogic.venueNames.size());

        //do what you want to the view here
        //for testing:
        return view;
    }//onCreateView

}//class
