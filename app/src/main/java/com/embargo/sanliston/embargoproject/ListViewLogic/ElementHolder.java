package com.embargo.sanliston.embargoproject.ListViewLogic;

import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

public class ElementHolder {
    //This is the holder class for the CustomListAdapter
    public SimpleDraweeView elementHolderImageView;
    public TextView elementHolderTitleView;
    public TextView elementHolderVisitsView;
    public TextView elementHolderOffersView;
    public TextView elementHolderDistanceView;

    public int position = 0;

}
