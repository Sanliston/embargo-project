package com.embargo.sanliston.embargoproject;

/***Sanliston Madzima Embargo Mini Project
 * Requirements Checklist Summary:
 * -Bottom navigation bar [partial complete]
 * -List of Venues with dynamically loading ListView [complete, although improvements can be made]
 * -Item consists of Image and a name [complete, although styling was rushed]
 * -Each item clickable [incomplete, ran out of time]
 * -Rounded corners [incomplete]
 * -Distance calculation [incomplete]
 * -Saving venues data to database [incomplete]
 * -Images and Video [incomplete, although can be provided on request]
 * ***/
import android.content.Context;
import android.graphics.Color;
import android.provider.CalendarContract;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.embargo.sanliston.embargoproject.ListViewLogic.CustomListAdapter;
import com.embargo.sanliston.embargoproject.ListViewLogic.ElementHolder;
import com.embargo.sanliston.embargoproject.ListViewLogic.GetVenueData;
import com.embargo.sanliston.embargoproject.ListViewLogic.ListViewLogic;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity{

    FragmentPagerAdapter adapterViewPager;
    ViewPager viewPager;
    ListView listView;


    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //applying the pager adapter
        viewPager = (ViewPager)findViewById(R.id.main_pager);
        adapterViewPager = new TabPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);

        /*I opted to use an external library: PagerSlidingTabStrip, as PagerTabStrip has some major limitations,
         * For example, it is very difficult to change the width of the tabs themselves without customizing the class.
          * Under normal circumstances I'd be happy to take a stab at this. But in this instance, it's a case of weighing
          * the time it would take to achieve this, and the reward. I opted for the safer option.*/
        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findViewById(R.id.main_tab_slider);
        tabStrip.setViewPager(viewPager);

        //********Continuing with network code***********
        Log.i("Main","About to launch the GetVenueData method");
        new GetVenueData().execute();

        CustomListAdapter customListAdapter = new CustomListAdapter(this, ListViewLogic.venueNames);
        ListView listView = (ListView) findViewById(R.id.browse_list_view);
        ListViewLogic.storage.put("customListAdapter", customListAdapter);

        //Using external Library Fresco for managing images
        Fresco.initialize(this);

    }


    //**********This section contains the setup for the FragmentPagerAdapter for the Tab Fragments****************
    public static class TabPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider{

        public static int PAGE_COUNT = 4;
        private int tabIcons[] = {R.drawable.baseline_home_black_18dp, R.drawable.baseline_room_black_18dp, R.drawable.baseline_group_work_black_18dp, R.drawable.baseline_person_black_18dp};

        public TabPagerAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        //This returns the count of pages
        @Override
        public int getCount(){
            return PAGE_COUNT;
        }

        //This returns the respective fragment for the page
        @Override
        public Fragment getItem(int position){
            switch(position){
                case 0:
                    return BrowseFragment.newInstance(0, "Browse"); //come back and change this, use a more elegant way of using strings
                case 1:
                    return MapFragment.newInstance(1,"Map");
                case 2:
                    return ExploreFragment.newInstance(2, "Explore");
                case 3:
                    return ProfileFragment.newInstance(3, "Profile");

                default:
                    return null;
            }
        }

        //For testing
        @Override
        public CharSequence getPageTitle(int position){
            return "Page" + position;
        }

        //for icons
        @Override
        public int getPageIconResId(int position){
            return tabIcons[position];
        }

    }//nested class - TabPagerAdapter

    //Handling when the back button is pressed
    @Override
    public  void onBackPressed(){
        if(viewPager.getCurrentItem()==0){
            super.onBackPressed();
        }else{
            viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
        }
    }



}
