package com.embargo.sanliston.embargoproject.ListViewLogic;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.embargo.sanliston.embargoproject.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

public class CustomListAdapter extends ArrayAdapter{

    //Values which where not provided by API, but I want to include anyway
    private final int offers = 0;
    private final int visits = 0;

    public CustomListAdapter (Activity context, ArrayList<String> names ){

        //Constructor Basically
        super(context, 0 /*R.layout.list_view_element*/, names);
        Log.i("CustomListAdapter","Initialized!");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        Log.i("CustomListAdapter","CustomListAdapter get view has ran");

        View view = convertView;
        ElementHolder elementHolder = new ElementHolder();

        //Makesure convertView isnt null, this is problematic
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_view_element, null);
            //SET VALUES FOR LAYOUT
            TextView titleView = (TextView) view.findViewById(R.id.list_element_name);
            TextView visitsView = (TextView) view.findViewById(R.id.list_element_visits);
            TextView offersView = (TextView) view.findViewById(R.id.list_element_offers);
            TextView distanceView = (TextView) view.findViewById(R.id.list_element_distance);
            SimpleDraweeView imageView = (SimpleDraweeView) view.findViewById(R.id.list_element_image);

            elementHolder.elementHolderTitleView = titleView;
            elementHolder.elementHolderVisitsView = visitsView;
            elementHolder.elementHolderOffersView = offersView;
            elementHolder.elementHolderDistanceView = distanceView;
            elementHolder.elementHolderImageView = imageView;

            view.setTag(elementHolder);

            Log.i("CustomListAdapter","CustomListAdapter get view has ran, and view is null");
        }else{
            elementHolder = (ElementHolder) view.getTag();
        }
        Log.i("CustomListAdapter","CustomListAdapter get view has ran, and view is not null");

        elementHolder.elementHolderTitleView.setText(ListViewLogic.getVenueName(position));
        elementHolder.elementHolderVisitsView.setText(0+" visits");
        elementHolder.elementHolderOffersView.setText(0+" offers");
        elementHolder.elementHolderDistanceView.setText(ListViewLogic.getVenueDistance(position)+" km");
        //image done using fresco
        String link = ListViewLogic.getVenueImage1URL(position);
        Log.i("CreateView","Uri : "+link);
        if(link==null){
            //use place holder image
        }else{
            Uri uri = Uri.parse(link);
            elementHolder.elementHolderImageView.setImageURI(uri);
        }

        return view;
    }


}
