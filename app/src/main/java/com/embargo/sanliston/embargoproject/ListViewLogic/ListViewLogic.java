package com.embargo.sanliston.embargoproject.ListViewLogic;

import android.app.ListActivity;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public class ListViewLogic extends ListActivity {
    //This is a class containing only static methods for handling the JSON data and pushing it to the ListView elements

    public static ArrayList<String> venueNames = new ArrayList<>();
    private static ArrayList<String> venueImage1URLs = new ArrayList<>();
    private static ArrayList<String> venueImage2URLs = new ArrayList<>();
    private static ArrayList<Double> venueLatitudes = new ArrayList<>();
    public static Long lastUpdate = null;
    public static Long lastUpdateAttempt = null;
    public static Boolean lastUpdateSuccessful = false;
    public static HashMap<String,Object> storage = new HashMap<>();
    private static int itemCount = 0;

    private static int initialElements = 2;

    public static void storeJsonData(String json) {
        try{
            parseJSON(json);
            CustomListAdapter customListAdapter = (CustomListAdapter) storage.get("customListAdapter");
            startFeed();
        }catch(Exception exception){
            //do something
            Log.i("ListViewLogic","Exception: "+exception.getMessage());

        }

        //String JSON = convertStreamToString(inputStreamReader);
        //Log.i("ListViewLogic", "JSON Value: "+JSON);
        lastUpdate = System.currentTimeMillis();
        lastUpdateSuccessful = true;
        Log.i("ListViewLogic","Update complete, size of arrayList: "+venueNames.size());


            //Do something to indicate that there is data.
    }

    public static void startFeed(){
        //This method controls how many elements are present in the ListView at any given time.
        //This limits the resources which ListView uses.
        //It does this by adding and removing elements to the feeding list as the user scrolls.

        //Add the initial 10 items
        CustomListAdapter customListAdapter = (CustomListAdapter) storage.get("customListAdapter");
        for(int i = 0; i<=initialElements ; i++){
            customListAdapter.add(venueNames.get(i));
        }

    }

    public static void continueFeed(){

    }



    private static void parseJSON(String json) throws JSONException{
        int cutOffindex = 0;
        String[] array = json.split("");

        //find index of "{"
        for(int i = 0; i<json.length(); i++){
            if(array[i].equals("{")){
                cutOffindex=i;
                break;
            }
        }

        //trim json
        json = json.substring(cutOffindex-1,json.length());
        json = json.replace("\\\"","'");
        Log.i("parseJSON","substring: "+json);
        JSONObject reader = new JSONObject(json);
        Log.i("parseJSON","got json object");


        //Get the object - In this case, that is: Venues
        JSONArray names = reader.getJSONArray("Venues");
        Log.i("parseJSON","got Venues Array");

        //Image URLS - Max of 3 images

        for(int i=0; i<names.length(); i++){
           JSONObject object = names.getJSONObject(i);
           String name = object.getString("Name");
            Log.i("For loop","name: "+name);

           JSONArray images = object.getJSONArray("Images");
           Log.i("Images","Image length: "+images.length());
           String image1 = images.getString(0);


           String latitude = object.getString("Latitude");
           Double numericLatitude = Double.parseDouble(latitude);
           Log.i("Numeric Lat: ","lat "+latitude);
           Log.i("Image URL:","image address "+image1);

           venueNames.add(name);
           venueImage1URLs.add(image1);
           //venueImage2URLs.add(image2);
           venueLatitudes.add(numericLatitude);

           itemCount++;
        }
    }


    public static String getVenueName(int index){

        if(index>=itemCount){
            return "";
        }
        return venueNames.get(index);
    }

    public static String getVenueImage1URL(int index){
        if(index>=itemCount){
            return "";
        }
        return venueImage1URLs.get(index);
    }

    public static Double getVenueDistance(int index){
        if(index>=itemCount){
            return 0.0;
        }
        return venueLatitudes.get(index);
    }


}
