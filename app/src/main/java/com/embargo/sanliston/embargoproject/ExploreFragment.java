package com.embargo.sanliston.embargoproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ExploreFragment extends Fragment {

    //instance variables
    private String title;
    private int page;

    //constructor
    public static ExploreFragment newInstance(int page, String title){
        ExploreFragment exploreFragment = new ExploreFragment();

        //storing values into bundle so they can be accessed later
        Bundle bundle = new Bundle();
        bundle.putInt("exploreInt", page);
        bundle.putString("exploreTitle", title);
        exploreFragment.setArguments(bundle);

        return exploreFragment;

    }//constructor

    //store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("exploreInt", 0);
        title = getArguments().getString("exploreTitle");
    }//onCreate

    //Inflate the view using the XML for the fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.explore_fragment, container, false); //come back to this part when you create the XML for the fragments

        //do what you want to the view here
        //for testing:
        TextView label = (TextView) view.findViewById(R.id.explore_text_view);
        label.setText("Page number: "+page+"  title: "+title);
        return view;
    }//onCreateView
}
