package com.embargo.sanliston.embargoproject.ListViewLogic;

import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.util.JsonReader;
import android.util.Log;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

public class GetVenueData extends AsyncTask<Void, Void, String> {
    /*This is the class that will be used to get data from the API given in the project specification*/

    @Override
    protected void onPreExecute(){
        super.onPreExecute();

        //Show progress bar on UI
        Log.i("GetVenueData","on execute launched");
    }

    @Override
    protected String doInBackground(Void...params){ //shout out to arbitrary parameter inputs

        String jsonString = null;
        try{
            //URL
            URL embargoAPI = new URL("https://dev.embargoapp.com/website/venues");

            //Establish Connection
            HttpsURLConnection APIConnection = (HttpsURLConnection) embargoAPI.openConnection();
            Log.i("GetVenueData","HTTPS URL connection achieved");
            //Provide user agent header
            APIConnection.setRequestProperty("User-Agent","sanliston-embargo-project");

            //Check response code
            if(APIConnection.getResponseCode() == 200){ //Any redirects such as 301 are handled automatically, so we need not worry about them
                //connection was successful
                Log.i("GetVenueData","Connection was successful");
                InputStream inputStream = APIConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");

                //Convert Stream to string
                Scanner scanner = new Scanner(inputStreamReader).useDelimiter("\\A");
                jsonString = scanner.hasNext()? scanner.next() :"";
                Log.i("GetVenueData","jsonString: "+jsonString);
            }

        }catch (Exception exception){
            //No need for anything here, because a null string will be returned.
            Log.i("GetVenueData","Connection was unsuccessful");
        }

        return jsonString;
    }

    @Override
    protected void onPostExecute(String response){
        //This sends the response to the ListViewLogic class if not null and invokes a contingency method if null.
        //These ArrayLists contain: venueNames , venueImageURL, and venueLatitude respectively.

        ArrayList<ArrayList> arrayList = new ArrayList<>();
        super.onPostExecute(response);
        Log.i("GetVenueData","Getting data done");

        if(response!=null){
            ListViewLogic.storeJsonData(response);
        }else{
            //contingency
            ListViewLogic.lastUpdateAttempt = System.currentTimeMillis();
            ListViewLogic.lastUpdateSuccessful = false;
        }
    }

}
