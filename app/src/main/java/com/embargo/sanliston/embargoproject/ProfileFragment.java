package com.embargo.sanliston.embargoproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ProfileFragment extends Fragment {

    //instance variables
    private String title;
    private int page;

    //constructor
    public static ProfileFragment newInstance(int page, String title){
        ProfileFragment profileFragment = new ProfileFragment();

        //storing values into bundle so they can be accessed later
        Bundle bundle = new Bundle();
        bundle.putInt("profileInt", page);
        bundle.putString("profileTitle", title);
        profileFragment.setArguments(bundle);

        return profileFragment;

    }//constructor

    //store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("profileInt", 0);
        title = getArguments().getString("profileTitle");
    }//onCreate

    //Inflate the view using the XML for the fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.profile_fragment, container, false); //come back to this part when you create the XML for the fragments

        //do what you want to the view here
        //for testing:
        TextView label = (TextView) view.findViewById(R.id.profile_text_view);
        label.setText("Page number: "+page+"  title: "+title);
        return view;
    }//onCreateView
}
